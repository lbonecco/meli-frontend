const axios = require("axios");
const express = require("express");
const app = express();
const port = 3001;
const author = {
  author: {
    name: "Leonardo",
    lastname: "Bonecco",
  },
};

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get("/api/items", (req, res) => {
  axios
    .get(`https://api.mercadolibre.com/sites/MLA/search?q=${req.query.q}`)
    .then((response) => {
      let categories = response.data.filters.map(
        (filter) => filter.id == "category" ? filter.values[0] : []
      );

      let items = response.data.results.map((result) => {
        return {
          id: result.id,
          title: result.title,
          price: {
            currency: result.prices.presentation.display_currency,
            amount: Math.round(result.prices.prices[0].amount),
            decimals:
              result.prices.prices[0].amount -
              Math.round(result.prices.prices[0].amount),
          },
          picture: result.thumbnail,
          condition: result.condition,
          free_shipping: result.shipping.free_shipping,
          address: result.address.state_name,
          category: result.category_id,
        };
      });

      res.json({
        author: author,
        categories: categories,
        items: items,
      });
    })
    .catch((error) => {
      console.log(error);
    });
});

app.get("/api/items/:id", (req, res) => {
  let url = encodeURI(
    `https://api.mercadolibre.com/items/​${req.params.id}`
  ).replace("%E2%80%8B", "");
  axios
    .all([axios.get(url), axios.get(`${url}/description`)])
    .then(
      axios.spread((resItem, resDescription) => {
        const item = {
          id: resItem.data.id,
          title: resItem.data.title,
          price: {
            currency: resItem.data.currency_id,
            amount: Math.round(resItem.data.price),
            decimals: resItem.data.price - Math.round(resItem.data.price),
          },
          picture: resItem.data.thumbnail,
          condition: resItem.data.condition,
          free_shipping: resItem.data.shipping.free_shipping,
          sold_quantity: resItem.data.sold_quantity,
          description: resDescription.data.plain_text,
        };

        

        axios
        .get(`https://api.mercadolibre.com/categories/${resItem.data.category_id}`)
    .then((rCategories) => {
      const categories = rCategories.data
      res.json({
        author: author,
        item: item,
        categories: categories,
      });
    }) 

        
      })
    )
    .catch((error) => {
      console.log(error);
    });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
