import { useState, useEffect } from "react";
import queryString from "query-string";
import { useLocation } from "react-router-dom";
import styled from "styled-components";

import { SearchBar } from "../components/SearchBar";
import { ResultList } from "../components/ResultList";
import { Breadcrumb } from "../components/Breadcrumb";
import { getResults } from "../service/apiService";

export const Results = () => {
  const location = useLocation();
  const { search = "" } = queryString.parse(location.search);

  const [items, setItems] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getResults(search).then((result) => {
      setItems(result.items.splice(0, 4));
      setCategories(result.categories.length? result.categories[0].path_from_root : []) ;
    });
  }, [search]);

  return (
    <>
      <SearchBar />
      <Section>
        <Breadcrumb categories={categories}/>
      </Section>
      <Section>
        <ResultList items={items} />
      </Section>
    </>
  );
};

/* Styles */

const Section = styled.section`
  padding: 0px 10%;
`;
