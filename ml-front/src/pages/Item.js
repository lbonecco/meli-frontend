import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import styled from "styled-components";

import { SearchBar } from "../components/SearchBar";
import { Breadcrumb } from "../components/Breadcrumb";
import { ItemDetails } from "../components/Items/ItemDetails";
import { getItemDetails } from "../service/apiService";

export function Item() {
  const [item, setItem] = useState([]);
  const [categories, setCategories] = useState([]);
  const { id } = useParams();
  useEffect(() => {
    getItemDetails(id).then((result) => {
      setItem(result.item);
      setCategories(result.categories.path_from_root) ;
    });
  }, []);

  return (
    <>
      <SearchBar />
      <Section>
        <Breadcrumb categories={categories}/>
      </Section>
      <Section>
        <ItemDetails item={item} />
      </Section>
    </>
  );
}

/* Styles */

const Section = styled.section`
  padding: 0px 10%;
`;
