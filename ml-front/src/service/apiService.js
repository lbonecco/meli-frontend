const server = "http://localhost:3001";

const getResults = async (query) => {
  const response = await fetch(`${server}/api/items?q=${query}`);
  const { items, categories } = await response.json();
  return { items, categories };
};

const getItemDetails = async (id) => {
  const response = await fetch(`${server}/api/items/${id}`);
  const detail = await response.json();
  return detail;
};

export { getResults, getItemDetails };
