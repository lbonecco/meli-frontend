import { useNavigate } from "react-router-dom";
import styled from "styled-components";

import shipping from "../../assets/icon_shipping.png";

export const ItemResult = ({ item }) => {

  const navigate = useNavigate();

  const handleClick = (e) => {
    e.preventDefault();
    navigate(`/items/${item.id}`)
  };

  return (
    <>
      <ItemContainer onClick={handleClick}>
        <ImgContainer>
          <img src={item.picture} alt="Product" />
        </ImgContainer>
        <DetailsContainer>
          <PriceContainer>
            <Price>
              {item.price.currency === "ARS" ? "$ " : "U$S "}
              {new Intl.NumberFormat("es-AR").format(item.price.amount)}
              {/* {result.free_shipping && ( */}
              <img alt="Free shipping" src={shipping} />
              {/* )} */}
            </Price>
            <Address>{item.address}</Address>
          </PriceContainer>
          <Title>{item.title}</Title>
        </DetailsContainer>
      </ItemContainer>
    </>
  );
};

/* Styles */

const ItemContainer = styled.article`
  padding: 16px 0px;
  background: #fff;
  display: flex;
  flex-direction: row;
  cursor: pointer;
`;

const DetailsContainer = styled.div`
  width: 100%;
  padding: 16px 0px;
`;

const ImgContainer = styled.div`
  padding: 0px 16px;
  img {
    width: 180px;
    height: 180px;
    border-radius: 4px;
  }
`;

const PriceContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 32px;
`;

const Price = styled.div`
  font-size: 24px;
  display: flex;
  align-items: center;
  gap: 10px;
`;

const Address = styled.div`
  font-size: 12px;
  width: 25%;
`;

const Title = styled.div`
  font-size: 18px;
`;
