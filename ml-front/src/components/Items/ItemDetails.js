import styled from "styled-components";

import shipping from "../../assets/icon_shipping.png";

export const ItemDetails = ({ item }) => {
  return (
    <>
      <ItemContainer>
        <ImgContainer>
          <img alt="Product" src={item.picture} />
        </ImgContainer>
        <InfoContainer>
          <Condition>
            {item.condition === "used" ? "Usado" : "Nuevo "}{" "}
            {item.sold_quantity > 0 && `- ${item.sold_quantity} vendidos`}
          </Condition>
          <h1>
            {item.title}{" "}
            {item.free_shipping && <img alt="shippingImg" src={shipping} />}
          </h1>
          <Price>
            {item.price?.currency === "ARS" ? "$ " : "U$S "}
            {new Intl.NumberFormat("es-AR").format(item.price?.amount)}{" "}
            <sup>
              {item.price?.decimals === 0
                ? "00"
                : item.price?.decimals.toString().substr(2, 2)}{" "}
            </sup>
          </Price>
          <button>Comprar</button>
        </InfoContainer>
      </ItemContainer>
      <DetailsContainer>
        <h2>Descripción del producto</h2>
        <p>{item.description}</p>
      </DetailsContainer>
    </>
  );
};

/* Styles */

const ItemContainer = styled.div`
  display: flex;
  gap: 16px;
  background: #fff;
  button {
    margin: 32px 32px 0px 0px;
    background: #3483fa;
    border: 0px;
    border-radius: 5px;
    color: #fff;
    padding: 10px 70px;
    cursor: pointer;
  }
`;

const ImgContainer = styled.div`
  img {
    width: 680px;
  }
`;

const InfoContainer = styled.div`
  h1 {
    font-size: 24px;
    margin-top: 16px;
  }
`;

const Condition = styled.div`
  margin-top: 32px;
  font-size: 14px;
`;

const Price = styled.div`
  display: flex;
  align-items: flex-start;
  font-size: 46px;
  margin: 32px 0px;
  sup {
    font-size: 26px;
  }
`;

const DetailsContainer = styled.div`
  background: #fff;
  padding: 16px 32px;
  h2 {
    font-size: 28px;
    margin-bottom: 32px;
  }
  p {
    font-size: 16px;
    padding-bottom: 32px;
    white-space: pre-wrap;
  }
`;
