import { ItemResult } from "./Items/ItemResult";

export const ResultList = ({ items }) => {
  return (
    <>
      {items.map((item) => {
        return <ItemResult item={item} />;
      })}
    </>
  );
};
