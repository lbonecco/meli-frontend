import styled from "styled-components";

export const Breadcrumb = ({ categories }) => {
  return (
    <BreadcrumbContainer>
      {categories.map((category, index) => {
        return (
          <>
            {index != 0 && <span> &gt; </span>}
            <span>{category.name}</span>
          </>
        );
      })}
    </BreadcrumbContainer>
  );
};

/* Styles */

const BreadcrumbContainer = styled.div`
  padding: 16px 0px;
  font-size: 14px;
  display: flex;
  gap: 10px;
`;
