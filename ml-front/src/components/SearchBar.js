import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";

import logo from "../assets/logo_ml.png";
import iconSearch from "../assets/icon_search.png";

export const SearchBar = () => {
  const navigate = useNavigate();

  const [search, setSearch] = useState("");
  const handleInputChange = ({ target }) => setSearch(target.value);

  const handleSearch = (e) => {
    e.preventDefault();
    navigate(`/items?search=${search}`);
  };

  const handleGoHome = (e) => {
    e.preventDefault();
    navigate(`/`);
  };

  return (
    <SearchContainer>
      <SearchWrapper>
        <Logo alt="Logo ML" src={logo} onClick={handleGoHome} />
        <From onSubmit={handleSearch}>
          <input
            type="text"
            name="search"
            onChange={handleInputChange}
            value={search}
            placeholder="Nunca dejes de buscar"
            autoCapitalize="off"
            autoCorrect="off"
            spellCheck="false"
          />
          <button>
            <IconSearch src={iconSearch} />
          </button>
        </From>
      </SearchWrapper>
    </SearchContainer>
  );
};

/* Styles */

const SearchContainer = styled.header`
  background-color: #fff159;
  padding: 5px 10%;
`;

const SearchWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: stretch;
  gap: 15px;
`;

const From = styled.form`
  display: flex;
  align-items: stretch;
  width: 90%;
  input {
    width: 90%;
    font-size: 18px;
    padding: 5px;
  }
  button {
    cursor: pointer;
  }
  * {
    border: 0px;
  }
`;

const Logo = styled.img`
  cursor: pointer;
`;
const IconSearch = styled.img``;
