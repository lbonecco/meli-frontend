import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

// import { ResultsScreen } from './components/results/ResultsScreen';
import { Index } from "./pages/Index";
import { Results } from "./pages/Results";
import { Item } from "./pages/Item";

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Index />} />
        <Route path="/items" element={<Results />} />
        <Route path="/items/:id" element={<Item />} />
        <Route
          path="*"
          element={
            <main>
              <p>404</p>
            </main>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};
